package com.cnetopro.mindidea.mapper;

import com.cnetopro.mindidea.entity.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户唯一ID表 Mapper 接口
 * </p>
 *
 * @author ZhuGuoXu
 * @since 2020-06-05
 */
@Mapper
public interface UsersMapper extends BaseMapper<Users> {

}

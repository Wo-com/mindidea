package com.cnetopro.mindidea.service;

import org.springframework.stereotype.Service;

@Service
public interface MindService {
    String getMind(Integer userID);
//    String getAllMind(Integer userID);
    String likeMind(Integer mindID,String str);
    String deleteMind(Integer userID,Integer mindID);
    String updateMind(Integer mindID,String lable,String content);
    String insertMind(Integer userID,String lable,String content);
}

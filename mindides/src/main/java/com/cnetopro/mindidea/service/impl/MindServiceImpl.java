package com.cnetopro.mindidea.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cnetopro.mindidea.entity.Minds;
import com.cnetopro.mindidea.mapper.MindsMapper;
import com.cnetopro.mindidea.service.MindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MindServiceImpl implements MindService {


    @Autowired
    MindsMapper mindsMapper;



    /**
     * 得到指定条数
     * */
    @Override
    public String getMind(Integer userID) {
        QueryWrapper<Minds> qwR=new QueryWrapper<>();
        qwR.eq("mind_user_id",userID);
        qwR.eq("mind_can_use",true);
        qwR.last("limit 0,12");             //1-12条数
        List<Minds> MindList= mindsMapper.selectList(qwR);

        if(MindList.size()==0){
            return "null";
        }

        String getMinds = JSON.toJSONString(MindList, SerializerFeature.WriteMapNullValue); // List转json
        return  getMinds;

    }

    /**
     * 模糊查询
     * */
    public String likeMind(Integer userID,String str) {
        //先查询用户所有想法
        System.out.println("模糊查询");
        QueryWrapper<Minds> qwMind=new QueryWrapper<>();
        qwMind.eq("mind_user_id",userID);
        qwMind.eq("mind_can_use",true);
        qwMind.like("mind_lable",str);
        List<Minds> mindList= mindsMapper.selectList(qwMind);

        if(mindList.size()==0){
            return "null";
        }

        String likeMinds = JSON.toJSONString(mindList, SerializerFeature.WriteMapNullValue); // List转json
        return  likeMinds;
    }




    /**
     *  伪删除
     * */

    @Override
    public String deleteMind(Integer userID,Integer mindID) {

        Minds mind=new Minds();
        mind.setMindId(mindID);
        mind.setMindCanUse(false);

        int res=mindsMapper.updateById(mind);

        if(res==1){
            return "ok";
        }else{
            return "error";
        }
    }

    /**
     * 更新
     * */
    @Override
    public String updateMind(Integer mindID,String lable,String content) {
        Minds mind=new Minds();
        mind.setMindId(mindID);
        mind.setMindLable(lable);
        mind.setMindContent(content);

        System.out.println(""+mind.toString());
        int res=mindsMapper.updateById(mind);
        if(res==1){
            return "ok";
        }else{
            return "error";
        }
    }

    /**
     * 插入
     * */
    @Override
    public String insertMind(Integer userID,String lable,String content) {

        Minds mind=new Minds();
        mind.setMindUserId(userID);
        mind.setMindLable(lable);
        mind.setMindContent(content);
        int res=mindsMapper.insert(mind);

        if(res==1){
            return "ok";
        }else{
            return "error";
        }
    }

}

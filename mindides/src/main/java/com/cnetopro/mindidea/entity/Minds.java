package com.cnetopro.mindidea.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户唯一ID表
 * </p>
 *
 * @author ZhuGuoXu
 * @since 2020-06-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Minds implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 想法id
     */
    @TableId(value = "mind_id", type = IdType.AUTO)
    private Integer mindId;

    /**
     * 用户ID
     */
    private Integer mindUserId;

    /**
     * 状态
     */
    private Boolean mindCanUse;

    /**
     * 想法标签
     */
    private String mindLable;

    /**
     * 想法具体内容
     */
    private String mindContent;

    /**
     * 创建时间
     */
    private LocalDateTime mindCreateTime;


}

package com.cnetopro.mindidea.controller;


import com.cnetopro.mindidea.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户唯一ID表 前端控制器
 * </p>
 *
 * @author ZhuGuoXu
 * @since 2020-05-25
 */
@RestController
public class UsersLoginController {
    @Autowired
    UserService uls;

    @RequestMapping("/login")
    @ResponseBody
    public String getcode(@RequestParam(value = "code")String  code){
        System.out.println("code"+code);
        String resp=uls.codeToOpenidAndSessionkey(code);    //发送请求获取opeind session
        String loginData=uls.login(resp);          //保存openID和session返回加密的session

        return loginData;
    }

    //保存用户信息
    @PostMapping ("/saveInfo")
    public String getall(String session,String userInfo){
        System.out.println("session"+session);
        System.out.println("userInfo"+userInfo);
        String res=uls.saveInfo(session, userInfo);

        return res;
    }



    @GetMapping("/test")
    String test(){
        return "test 成功";
//        return "test 测试";
    }

}

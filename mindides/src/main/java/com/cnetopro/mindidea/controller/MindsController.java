package com.cnetopro.mindidea.controller;


import com.cnetopro.mindidea.service.MindService;
import com.cnetopro.mindidea.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户唯一ID表 前端控制器
 * </p>
 *
 * @author ZhuGuoXu
 * @since 2020-05-25
 */
@RestController
public class MindsController {

    @Autowired
    MindService mindService;

    @Autowired
    UserService userService;


    /**
     * 查询部分
     * */
    @PostMapping ("/getmind")
    public String getall(String session){
        System.out.println("session"+session);
        int userID=userService.getUserID(session);
        String resp=mindService.getMind(userID);
        return resp;
    }

    /**
     * 查询全部
     * */
    @PostMapping ("/savemind")
    public String savemind(String session,int mindid, String title,String context){
        System.out.println("session "+session);
        System.out.println("title "+title);
        System.out.println("context "+context);

        int userID=userService.getUserID(session);
        String resp=mindService.insertMind(userID,title,context);
        return resp;
    }
    /**
     * 查询全部
     * */
    @PostMapping ("/likemind")
    public String getMindlike(String session,String str){
        System.out.println("session "+session);
        System.out.println("str "+str);

        int userID=userService.getUserID(session);
        String resp=mindService.likeMind(userID,str);
        return resp;
    }



    /**
     * 更新想法
     * */
    @PostMapping ("/updatemind")
    public String updatemind(String session,Integer mindid, String title,String context){
        System.out.println("session "+session);
        System.out.println("ID "+mindid);
        System.out.println("title "+title);
        System.out.println("context "+context);

        String resp=mindService.updateMind(mindid,title,context);
        return resp;
    }

    @PostMapping ("/deletemind")
    public String deletemind(String session,Integer mindid){
        System.out.println("session "+session);
        System.out.println("ID "+mindid);

        int userID=userService.getUserID(session);
        String resp=mindService.deleteMind(userID,mindid);
        return resp;
    }

}

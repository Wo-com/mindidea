package com.cnetopro.mindidea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MindideaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MindideaApplication.class, args);
    }

}

-- 创建数据库
drop database if exists mindidea;
CREATE SCHEMA `mindidea` DEFAULT CHARACTER SET utf8 ;
-- 已经创建 被注释

-- 使用创建好的数据库
use mindidea;

-- -----------------------------------------------------------------------------
-- 1、用户唯一ID与openid表
-- -----------------------------------------------------------------------------
drop table if exists users;
create table users(
  user_id 		    int(11) 	        not null auto_increment    comment '用户id',
  user_open_id 		varchar(64) 	    unique  			       comment 'openid',
  user_session 	   	varchar(64) 	    not null   			       comment '加密后的session',
  user_nickname     varchar(60)                                    comment '用户昵称',   
  user_head_url     varchar(2083)                                  comment '用户头像地址', 
  user_create_time 	timestamp           default current_timestamp  comment '创建时间',
  primary key (user_id),
  unique key (user_open_id)
) engine=innodb auto_increment=1        default charset=utf8       comment = '用户唯一ID表';


-- -----------------------------------------------------------------------------
-- 2、想法表
-- -----------------------------------------------------------------------------
drop table if exists minds;
create table minds (
  mind_id 		    int(11) 	        not null auto_increment    comment '想法id',
--  mind_user_id      int(11)             not null                   comment '用户ID',
--  mind_can_use      tinyint(1)          default '1'				   comment '状态',
--  mind_null         varchar(100)								   comment '预留字段',
  mind_lable 		varchar(1023) 	    not null  			       comment '想法标签',
  mind_content 	   	text        	            			       comment '想法具体内容',
  mind_create_time 	timestamp           default current_timestamp  comment '创建时间',
  primary key (mind_id)
) engine=innodb auto_increment=1        default charset=utf8       comment = '用户唯一ID表';


-- -----------------------------------------------------------------------------
-- 3、关系表
-- -----------------------------------------------------------------------------
drop table if exists user_r_mind;
create table user_r_mind (
 user_r_mind_id    bigint(11) 		    not null auto_increment    comment '用户想法关系id',
 user_id 		   int(11) 	            not null                   comment '用户id',
 mind_id 		   int(11)      	    not null  			       comment '想法id',
 mind_can_use      tinyint(1)           default '1'				   comment '状态',
 primary key (user_r_mind_id),
 foreign key(user_id) references users(user_id),
 foreign key(mind_id) references minds(mind_id)
) engine=innodb auto_increment=1        default charset=utf8       comment = '用户想法关系表';
